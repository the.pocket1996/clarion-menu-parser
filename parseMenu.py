menuFile = open('menu.txt','r')
import re
re.IGNORECASE = True

def  getText(s,start,end,db=0):
    #s=s.lower()
    if db == 1:
        print(f'[getText]: s = {s} start = {start} end = {end}')
        print(f'[getText]: find 1 = {s.find(start)+len(start)} find 2 = {s.find(end)}')
        print(f'[getText]: returning {s[s.find(start)+len(start):s.find(end,s.find(start)+len(start))]}')
    return s[s.find(start)+len(start):s.find(end,s.find(start)+len(start))]

def buildPath(item):
    retVal = ""
    for cp in currentPath:
        retVal = retVal + (" > " if len(retVal) > 1 else "") + cp
    return retVal + (" > " if len(retVal) > 1 else "")  + item


items = {}
currentPath=[]

for line in menuFile:
#    print(line,end='')
    if line.strip().lower().startswith("menu"):
        items[getText(line,"USE(",")")] = buildPath(getText(line,"MENU('","')")) + " > "
        currentPath.append(getText(line,"MENU('", "')"))
    if line.strip().lower().startswith("END"):
        currentPath.pop()
    if line.strip().lower().startswith("ITEM"):
        items[getText(line,"USE(",")")] = buildPath(getText(line,"ITEM('","')"))
    #print(f'currentPath = {currentPath}')
#    for i in range(len(line)):
#        print(f'char =  {line[i]}')

fileOut = open("parsed.txt","w")


for item in items:
    print(f'{item} : "{items[item]}"')
    fileOut.write(f'{item} : "{items[item]}"\n')
fileOut.close()

#keepGoing = True
#itemToGet = ""
#while keepGoing:
    #itemToGet = input("enter a control ID e.g. ?RSE or exit to leave")
    #if itemToGet.lower() == "exit":
        #keepGoing = False
        #break
    #if itemToGet.startswith('?') == False:
        #itemToGet = '?'+itemToGet
    #print(items[itemToGet])

#print(items['?sub1'])
print("End Of Execution Parsed.txt created.")
